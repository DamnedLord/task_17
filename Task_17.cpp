#include <iostream>
#include <ctgmath>

#pragma region SquareSum(...)
//###################################
template<typename T>
constexpr T SquareSum(T t)
{
	return t * t;
}

// Сумма квадратов аргументов
template<typename T, typename ...Ts>
constexpr T SquareSum(T t, Ts ...ts)
{
	return t * t + SquareSum(ts...);
}
//###################################
#pragma endregion



template<typename T>
class VectorT
{
public:
	typedef T value_type;

public:
	// Конструкторы
	VectorT() : x(0), y(0), z(0)
	{
	}

	VectorT(T _x, T _y, T _z) : x(_x), y(_y), z(_z)
	{
	}

	VectorT(const VectorT &rhs) : x(rhs.x), y(rhs.y), z(rhs.z)
	{
	}

	VectorT(VectorT &&rhs) : x(rhs.x), y(rhs.y), z(rhs.z)
	{
	}

	// Операторы
	VectorT & operator= (const VectorT &rhs)
	{
		x = rhs.x;
		y = rhs.y;
		z = rhs.z;

		return *this;
	}

	VectorT& operator= (VectorT &&rhs)
	{
		x = rhs.x;
		y = rhs.y;
		z = rhs.z;

		return *this;
	}

	// Методы

	// Длина (магнитуда) вектора
	T Length() const
	{
		return sqrt( SquareSum(x, y, z) );
	}

	// Магнитуда (длина) вектора
	T Magnitude() const
	{
		return Length();
	}

	// Вывод вектора в консоль
	void Show() const
	{
		std::cout << "x = " << x << '\t' << "y = " << y << '\t' << "z = " << z << std::endl;
	}

private:
	T x;
	T y;
	T z;
};

typedef VectorT<double> Vector;
typedef VectorT<float> VectorF;
typedef VectorT<long double> VectorL;

class Transform
{
public:
	Transform() {}

	Transform(const Vector &_location, const Vector &_rotation, const Vector &_scale) :
		location(_location),
		rotation(_rotation),
		scale(_scale)
	{}

	Transform(Vector &&_location, Vector &&_rotation, Vector &&_scale) :
		location(std::forward<Vector>(_location)),
		rotation(std::forward<Vector>(_rotation)),
		scale(std::forward<Vector>(_scale))
	{}

public:
	const Vector & GetLocation() const
	{
		return location;
	}

	void SetLocation(const Vector &_location)
	{
		location = _location;
	}

	void SetLocation(Vector &&_location)
	{
		location = std::forward<Vector>(_location);
	}


	const Vector & GetRotation() const
	{
		return rotation;
	}

	void SetRotation(const Vector &_rotation)
	{
		rotation = _rotation;
	}

	void SetRotation(Vector &&_rotation)
	{
		rotation = std::forward<Vector>(_rotation);
	}


	const Vector & GetScale() const
	{
		return scale;
	}

	void SetScale(const Vector &_scale)
	{
		scale = _scale;
	}

	void SetScale(Vector &&_scale)
	{
		scale = std::forward<Vector>(_scale);
	}

private:
	Vector location;
	Vector rotation;
	Vector scale;
};


int main()
{
	const Vector v1(1, 1, 1);	// Реализация для double
	const VectorF v2(2, 2, 2);	// Реализация для float
	const VectorL v3(3, 3, 3);	// Реализация для long double, но в MS VC++ соответствует double

	v1.Show();
	std::cout << "Length = " << v1.Length() << std::endl;

	v2.Show();
	std::cout << "Length = " << v2.Magnitude() << std::endl;

	v3.Show();
	std::cout << "Length = " << v3.Length() << std::endl;



	Transform tr{
		{1.2, 1.2, 1.2},	// location
		{2.3, 2.3, 2.3},	// rotation
		{3.4, 3.4, 3.4}		// scale
	};

	std::cout << std::endl << "Transform::GetLocation" << std::endl;
	tr.GetLocation().Show();

	std::cout << std::endl << "Transform::SetLocation" << std::endl;
	v1.Show();
	tr.SetLocation(v1);

	std::cout << std::endl << "Transform::GetLocation" << std::endl;
	tr.GetLocation().Show();
}